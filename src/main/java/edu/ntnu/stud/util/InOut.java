package edu.ntnu.stud.util;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * <h2>InOut</h2>
 * <code>InOut</code> is a utility class with static methods for improved input and 
 * output.
 * 
 * <h3>Role</h3>
 * The role of this class is to provide utility methods that can be used throughout the application.
 * These methods are static, meaning they can be called without creating an instance of the 
 * <code>Utils</code> class.
 * 
 * <h3>Responsibility</h3>
 * The class is responsible for providing utility methods such as <code>getInput</code> which is 
 * used to get user input in a safe and controlled manner as well as <code>format</code> which is 
 * used to format strings for improved output.
 *
 * @author Erik Hoff
 * @version 1.0
 */
public class InOut {

  // input
  /**
   * Gets input from the user. The input is parsed if possible and checked if it is in the provided 
   * range. If the input is invalid, the user is prompted to enter a new input. This method
   * currently only supports the following classes: <code>Integer</code>, <code>Long</code>,
   * <code>Double</code>, <code>Float</code> and <code>String</code>. 
   *
   * @param <T> the type of input
   * @param target the target class to parse to
   * @param input the scanner to read from
   * @param min the minimum value (inclusive)
   * @param max the maximum value (inclusive)
   * @param message the message to display
   * @return the input
   * @throws IllegalArgumentException if the target class is invalid
   */
  public static <T> T getInput(Class<T> target, Scanner input, int min, int max, String message)
      throws IllegalArgumentException {
    // Print message
    System.out.println(
        message 
        + " ( " + target.getSimpleName() 
        + " in range [" + min + "," + max + "] )"
    );

    String rawIn;
    System.out.println("");
    do {
      System.out.print("\033[1A"); // Move up in console
      System.out.print("\033[2K"); // Erase line content in console

      // Request input
      rawIn = input.nextLine();

      // Check if input can be parsed
      if (!canParseInput(target, rawIn)) {
        continue;
      }

      // Parse input
      T parsed = parseInput(target, rawIn);

      // Check if input is in range
      if (isInRange(parsed, min, max)) {
        // return parsed input
        return parsed;
      }
    } while (true);
  }

  /**
   * Checks if the string input can be parsed to the target class. This method currently only 
   * supports regexes for the following classes: <code>Integer</code>, <code>Long</code>, 
   * <code>Double</code>, <code>Float</code> and <code>String</code>.
   *
   * @param <T> the type of the target class
   * @param target the target class to parse to
   * @param rawIn the string to check
   * @return true if the input can be parsed to the target class
   * @throws IllegalArgumentException if the target class is invalid
   */
  private static <T> boolean canParseInput(
      Class<T> target, String rawIn
  ) throws IllegalArgumentException {
    // regexes
    final HashMap<Class<?>, String> regexes = new HashMap<>();
    regexes.put(Integer.class, "[0-9]+");
    regexes.put(Long.class, "[0-9]+");
    regexes.put(Double.class, "[0-9]+(\\.[0-9]+)?");
    regexes.put(Float.class, "[0-9]+(\\.[0-9]+)?");
    regexes.put(String.class, ".*");

    // Check if target class is valid and corresponding find regex
    String pattern = regexes.get(target);
    if (pattern == null) {
      throw new IllegalArgumentException("Invalid target class");
    }

    return Pattern.matches(pattern, rawIn);
  }

  /**
   * Checks if the in paramater is in the specified range. This method currently only supports
   * ranges for the following classes: <code>Integer</code>, <code>Long</code>,
   * <code>Double</code>, <code>Float</code> and <code>String</code>.
   *
   * @param <T> the type of the target class
   * @param in the input to check
   * @param min the minimum value (inclusive)
   * @param max the maximum value (inclusive)
   * @return true if the input is in the specified range
   * @throws IllegalArgumentException if the target class is invalid
   */
  private static <T> boolean isInRange(T in, int min, int max) throws IllegalArgumentException {
    final HashMap<Class<?>, Predicate<Object>> checkArguments = new HashMap<>();
    checkArguments.put(Integer.class, i -> ((Integer) i) >= min && ((Integer) i) <= max);
    checkArguments.put(Long.class, i -> ((Long) i) >= min && ((Long) i) <= max);
    checkArguments.put(Double.class, i -> ((Double) i) >= min && ((Double) i) <= max);
    checkArguments.put(Float.class, i -> ((Float) i) >= min && ((Float) i) <= max);
    checkArguments.put(
        String.class, i -> ((String) i).length() >= min && ((String) i).length() <= max
    );

    Predicate<Object> check = checkArguments.get(in.getClass());
    if (check == null) {
      throw new IllegalArgumentException("Invalid target class");
    }
    return check.test(in);
  }

  /**
   * Represents a parse method.
   */
  public interface Parse<T> {
    T parse(String s);
  }

  /**
   * Parses the input to the target class. This method currently only supports parsing for the
   * following classes: <code>Integer</code>, <code>Long</code>, <code>Double</code>,
   * <code>Float</code> and <code>String</code>.
   *
   * @param <T> the type of the target class
   * @param target the target class to parse to
   * @param rawIn the string to parse
   * @return the parsed input
   */
  private static <T> T parseInput(Class<T> target, String rawIn) {
    final HashMap<Class<?>, Parse<?>> parseArguments = new HashMap<>();
    parseArguments.put(Integer.class, Integer::parseInt);
    parseArguments.put(Long.class, Long::parseLong);
    parseArguments.put(Double.class, Double::parseDouble);
    parseArguments.put(Float.class, Float::parseFloat);
    parseArguments.put(String.class, String::toString);

    Parse<?> parse = parseArguments.get(target);
    if (parse == null) {
      throw new IllegalArgumentException("Invalid target class");
    }
    return target.cast(parse.parse(rawIn));
  }

  // string formatting
  /**
   * Returns true if the specified string is in the specified array.
   *
   * @param arr the array to search
   * @param elm the string to search for
   * @return true if the specified string is in the specified array
   */
  public static boolean findString(String[] arr, String elm) {
    for (String object : arr) {
      if (object.equals(elm)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns a string with the specified format and arguments. The format string is a string with
   * formats in the following syntax: <code>%[number][prefix][command]</code>. The number is the 
   * length of the argument. The prefix is a single character that specifies how the argument 
   * should be formatted. The following prefixes are supported: 
   * <ul>
   * <li><code>l</code> for left padding,</li>
   * <li><code>r</code> for right padding,</li>
   * <li><code>s</code> for sentering padding,</li>
   * <li><code>d</code> for digit padding.</li>
   * </ul>
   * The command is a single character that specifies additional formatting, like padding. The
   * following commands are supported:
   * <ul>
   * <li><code>u</code> for upper case,</li>
   * <li><code>l</code> for lower case,</li>
   * <li><code>t</code> for title case,</li>
   * <li><code>n</code> for name case.</li>
   * </ul>
   *
   * <p>
   * The following is an example of a format string: <code>"%8r"</code>. This format string will
   * format the argument with left padding to a length of 10. A argument with the value "Hello" will
   * be formatted to <code>"Hello   "</code>, with 3 spaces after the word, and a argument with the 
   * value "Hello World" will be formatted to <code>"Hello..."</code>.
   * </p>
   * 
   * <p>
   * This method is currently not to the same coding standard as the rest of the class and has 
   * oppertunity for improvement. The functionality is however complete and works as intended.
   * </p>
   *
   * @param format the format string
   * @param args the arguments
   * @return the formatted string
   * @throws IllegalArgumentException if the format string is invalid
   */
  public static String format(String format, String... args) throws IllegalArgumentException {
    final String[] numbers = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}; // numbers
    final String[] commands = {"l", "r", "s", "d"}; // paddin: left, right, senter, digit
    final String[] prefixes = {"t", "u", "l", "n"}; // title, upper, lower, name
    String result = ""; // result string, fullly formatted
    String readNummber = ""; // number being read
    String arg = ""; // current argument being formatted
    int argIndex = 0; // current argument index
    boolean inFormat = false; // if currently reading a format string or normal string
    boolean readingPrefix = false; // if currently reading a prefix
    String[] characters = format.split(""); // split format string into characters
    for (int i = 0; i < characters.length; i++) { 
      /* itterate through each character. We use for loops here instead of other alternativves 
      because we need the itteration index */
      String c = characters[i]; // current character
      if (inFormat) {
        if (readingPrefix) {
          if (findString(prefixes, c)) { // a later prefix overrides an earlier prefix in conflicts

            if (c.equals("t")) { // title case
              arg = arg.substring(0, 1).toUpperCase() + arg.substring(1).toLowerCase();   
            }
            if (c.equals("n")) { // name case
              String[] words = arg.split(" ");
              String name = "";
              for (String string : words) {
                if (string.length() <= 2) {
                  name += string.toLowerCase() + " ";
                  continue;
                }
                name += 
                      string.substring(0, 1).toUpperCase() 
                    + string.substring(1).toLowerCase() 
                    + " ";
              }
              arg = name;
            }
            if (c.equals("u")) { // upper case
              arg = arg.toUpperCase();
            }
            if (c.equals("l")) { // lower case
              arg = arg.toLowerCase();
            }
          } else {
            readingPrefix = false;
          }
        }
        if (!readingPrefix) { // if not reading prefix read number or command
          if (findString(numbers, c)) { // if number then read number
            readNummber += c;
          } else if (findString(commands, c)) { // if command then format
            inFormat = false;
            int length = Integer.parseInt(readNummber);
            if (argIndex >= args.length) {
              throw new IllegalArgumentException("Too few arguments");
            }

            if (c.equals("l")) { // string with left padding and fixed length
              if (arg.length() > length) {
                if (length <= 3) { // A string of length 3 or less should not be truncated
                  throw new IllegalArgumentException(
                    "argument out of length for a short format at " + (i - readNummber.length() - 1)
                    );
                }
                arg = arg.substring(0, length - 3) + "..."; // truncate the argument
              }
              for (int j = 0; j < (length - arg.length()); j++) {
                result += " "; // add padding
              }
              result += arg; // add argument
            }

            if (c.equals("r")) { // string with right padding and fixed length
              if (arg.length() > length) {
                if (length <= 3) { // A string of length 3 or less should not be truncated
                  throw new IllegalArgumentException(
                    "argument out of length for a short format at " + (i - readNummber.length() - 1)
                  );
                }
                arg = arg.substring(0, length - 3) + "..."; // truncate the argument
              }
              result += arg; // add argument
              for (int j = 0; j < (length - arg.length()); j++) {
                result += " "; // add padding
              }
            }

            if (c.equals("s")) { // string with sentering padding and fixed length. 
              // If padding is uneven then extra padding is added to the right
              if (arg.length() > length) {
                if (length <= 3) { // A string of length 3 or less should not be truncated
                  throw new IllegalArgumentException(
                    "argument out of length for a short format at " + (i - readNummber.length() - 1)
                    );
                }
                arg = arg.substring(0, length - 3) + "..."; // truncate the argument
              }
              int padding = (length - arg.length());
              int paddingRight = padding / 2;
              int paddingLeft = padding - paddingRight;
              for (int j = 0; j < (paddingLeft); j++) {
                result += " "; // add padding
              }
              result += arg; // add argument
              for (int j = 0; j < (paddingRight); j++) {
                result += " "; // add padding
              }
            }

            if (c.equals("d")) { // number with fixed amount of digits
              if (arg.length() > length) { // cannot truncate a number
                throw new IllegalArgumentException(
                  "argument out of length for a digit format at " + (i - readNummber.length() - 1)
                );
              }
              for (int j = 0; j < (length - arg.length()); j++) {
                result += "0"; // add padding
              }
              result += arg; // add argument
            }

            argIndex += 1;
            readNummber = "";
          } else {
            throw new IllegalArgumentException(
              "Invalid format string at index " + (i - readNummber.length() - 1)
              );
          }
        }
  
      } else {
        if (c.equals("%")) {
          inFormat = true;
          readingPrefix = true;
          if (argIndex >= args.length) {
            throw new IllegalArgumentException("Too few arguments");
          }
          arg = args[argIndex];
        } else {
          result += c;
        }
      }
    }
    return result;
  }
    
}
