package edu.ntnu.stud;

import edu.ntnu.stud.ui.TrainDispatchApp;

/**
 * <h2>Main</h2>
 * The <code>Main</code> class is the entry point of the application. It is responsible for 
 * creating, initializing and starting the <code>TrainDispatchApp</code>. The app i filled with
 * dummy data for testing purposes.
 *
 * @author Erik Hoff
 * @version 1.0
 */
public final class Main {
  /**
   * Sets up and starts the application.
   *
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    TrainDispatchApp t = new TrainDispatchApp(true);
    t.init();
    t.start();
  }
}
