package edu.ntnu.stud.ui;

import edu.ntnu.stud.models.Clock;
import edu.ntnu.stud.models.Departure;
import edu.ntnu.stud.models.DispatchSystem;
import edu.ntnu.stud.util.InOut;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * <h2>TrainDispatchApp</h2>
 * Represents a train dispatch application. It is responsible for interacting with the user 
 * through a console-based menu system. The class manages the dispatch system and handles 
 * user input for dispatching trains.
 * 
 * <h3>Role</h3>
 * The role of this class is to serve as the main interface between the user and the dispatch 
 * system. It prompts the user for input and processes the input to perform various operations 
 * such as scheduling departures etc.
 * 
 * <h3>Responsibility</h3>
 * The class is responsible for managing the state of the dispatch system and ensuring that the 
 * user's commands are executed correctly. It also handles any errors that may occur 
 * during the execution of the user's commands. The application is also responsible for
 * filling the dispatch system with generated data from predefined arrays of destinations and
 * lines.
 *
 * @author Erik Hoff
 * @version 1.0
 */
public class TrainDispatchApp {
  private Scanner input;
  private boolean autoSeed;
  private boolean running;
  private DispatchSystem data;
  static String[] destinations = {"Oslo S", "Bergen", "Trondheim", "Ski", "Stavanger", "Drammen", 
      "Lillehammer", "Kristiansand", "Bodø", "Tromsø", "Hamar", "Halden", "Moss", "Sandefjord",
      "Ålesund", "Gjøvik", "Molde", "Narvik", "Harstad", "Larvik", "Horten", "Kongsberg", 
      "Haugesund", "Sandnes", "Tønsberg", "Arendal", "Porsgrunn", "Bærum", "Ytre Enebakk", 
      "Lørenskog", "Skedsmo", "Nittedal", "Fredrikstad", "Sarpsborg", "Ringsaker"};
  static String[] lines = {"L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8", "F1", "F2", "F3", "F4", 
      "F5", "F6", "F7", "F8"};

  // constructors
  public TrainDispatchApp(boolean autoSeed) {
    setAutoSeed(autoSeed);
    setRunning(false);
  }

  public TrainDispatchApp() {
    setAutoSeed(false);
    setRunning(false);
  }

  // getters
  public Scanner getInput() {
    return input;
  }

  public boolean getAutoSeed() {
    return autoSeed;
  }

  public boolean getRunning() {
    return running;
  }

  public DispatchSystem getData() {
    return data;
  }

  // setters
  public void setAutoSeed(boolean autoSeed) {
    this.autoSeed = autoSeed;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  private void setData(DispatchSystem data) {
    this.data = data;
  }

  private void setInput(Scanner input) {
    this.input = input;
  }
  
  // methods
  /**
   * Seeds the application with a random DispatchSystem.
   *
   * @return the seeded DispatchSystem
   */
  private DispatchSystem seed() {
    final Random rand = new Random();

    DispatchSystem ds = new DispatchSystem(new ArrayList<Departure>());
    for (int i = 0; i < 10; i++) {
      ds.addDeparture(new Departure(new Clock(rand.nextInt(24), 
          rand.nextInt(60)), 
          lines[rand.nextInt(lines.length)], 
          i + 1,
          destinations[rand.nextInt(destinations.length)], 
          rand.nextBoolean() ? rand.nextInt(10) + 1 : -1,
          new Clock(
            rand.nextBoolean() ? rand.nextInt(3) : 0, 
            rand.nextBoolean() ? rand.nextInt(60) : 0
          )
      ));
    }
    return ds;
  }

  /**
   * Initializes the application.
   */
  public void init() {
    System.out.println("Initializing...");
    setInput(new Scanner(System.in));
    Departure[] departures = {};
    setData(this.autoSeed ? seed() : new DispatchSystem(departures));
  }

  /**
   * Starts the application.
   */
  public void start() {
    System.out.println("Starting...");
    this.running = true;

    while (running) {

      // print menu
      System.out.println("\n -- Choose an option: -- \n"
          + "1. List departures\n"
          + "2. Add departure\n"
          + "3. Remove departure\n"
          + "4. Assign track to departure\n"
          + "5. Set delay at departure\n"
          + "6. Find departure by ID\n"
          + "7. Find departure by Destination\n"
          + "8. Set clock\n"
          + "9. Re-seed\n"
          + "0. Exit\n"
          + "-> ");

      // get choice
      int choice = InOut.getInput(Integer.class, input, 0, 9, "Enter choice: ");

      // do corresponding action
      switch (choice) {
        case 0: {
          running = false;
        }
        break;

        case 1: {
          System.out.println(data.toString());
        }
        break;

        case 2: {
          Departure newDeparture = new Departure(
              new Clock(
                  InOut.getInput(Integer.class, input, 0, 23, "Enter hour of departure: "),
                  InOut.getInput(Integer.class, input, 0, 59, "Enter minute of departure: ")
              ),
              InOut.getInput(String.class, input, 1, 4, "Enter line name: "),
              InOut.getInput(Integer.class, input, 0, 9999, "Enter uniqe train ID: "),
              InOut.getInput(String.class, input, 1, 15, "Enter destination: ")
          );
          if (!data.addDeparture(newDeparture)) {
            System.out.println("Departure ID already exists");
          }
        }
        break;

        case 3: {
          if (!data.removeDepartureById(InOut.getInput(Integer.class, input, 0, 99990, 
              "Enter ID of departure to remove: "))) {
            System.out.println("No departure with that ID");
          }
        }
        break;

        case 4: {
          Departure departure = data.getDepartureById(InOut.getInput(Integer.class, input, 0, 
              99999, "Enter ID of departure to add track to: "));
          if (departure != null) {
            departure.setTrack(InOut.getInput(Integer.class, input, 0, 99999, 
                "Enter track number: "));
          } else {
            System.out.println("No departure with that ID");
          }
        }
        break;

        case 5: {
          Departure departure = data.getDepartureById(InOut.getInput(Integer.class, input, 0, 
              99999, "Enter ID of departure to apply delay to: "));
          if (departure != null) {
            departure.setDelay(
                new Clock(
                    InOut.getInput(Integer.class, input, 0, 23, "Enter hour of delay: "),
                    InOut.getInput(Integer.class, input, 0, 59, "Enter minute of delay: ")
                )
            );
            data.updateClock(data.getClock()); // update clock to remove old departures
          } else {
            System.out.println("No departure with that ID");
          }
        }
        break;

        case 6: {
          Departure departure = data.getDepartureById(InOut.getInput(Integer.class, input, 0, 
              99999, "Enter ID of departure to find: "));
          if (departure != null) {
            // make new DispatchSystem with only the found departure and print it for a nicer format
            ArrayList<Departure> departures = new ArrayList<Departure>();
            departures.add(departure);
            System.out.println(new DispatchSystem(departures, new Clock()).toString());
          } else {
            System.out.println("No departure with that ID");
          }
        }
        break;

        case 7: {
          Departure[] departures = data.getDepartureByDestination(InOut.getInput(String.class, 
              input, 1, 20, "Enter destination of departure to find: "));
          if (departures.length > 0) { 
            // make new DispatchSystem with only the found departure and print it for a nicer format
            System.out.println(new DispatchSystem(departures, new Clock()).toString());
          } else {
            System.out.println("No departure with that destination");
          }
        }
        break;

        case 8: {
          if (!data.updateClock(
              new Clock(
                  InOut.getInput(Integer.class, input, 0, 23, "Enter hour of new clock time: "),
                  InOut.getInput(Integer.class, input, 0, 59, "Enter minute of new clock time: ")
              )
          )) {
            System.out.println("New clock time is before current clock time");
          }
        }
        break;

        case 9: {
          data = seed();
        }
        break;

        default:
          System.out.println("An error ocured. Continuing...");
      }
    }
    
    // exit
    System.out.println("Exiting...");
    input.close();
  }
}
