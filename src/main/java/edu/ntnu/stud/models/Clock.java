package edu.ntnu.stud.models;

import edu.ntnu.stud.util.Validate;

/**
 * <h2>Clock</h2>
 * Represents a clock with hour and minute fields. It can also function as a time unit.
 * The values are clamped between 00:00 and 23:59, representing the 24-hour format of a day. The 
 * values are clamped using the modulo operator and and excess of minutes are not converted and 
 * added to the hour field.
 * <p>
 * <b>Note:</b> This class throws an IllegalArgumentException if either the hour or minute 
 * value is negative during construction or when setting the values.
 * </p>
 * <h3>Responsibilities:</h3>
 * The responsibilities of the class include storing, retrieving and manipulating the hour and
 * minute values of a clock. It also provides methods for comparing and represent two Clock objects.
 *
 * @author Erik Hoff
 * @version 1.0
 */
public class Clock { 
  int hour;
  int minute;

  // constructors
  /**
   * Constructs a new Clock object with the specified hour and minute values.
   *
   * @param hour the hour value
   * @param minute the minute value
   * @throws IllegalArgumentException if either value is negative
   */
  public Clock(int hour, int minute) throws IllegalArgumentException {
    setHour(hour);
    setMinute(minute);
  }

  /**
   * Constructs a new Clock object with the specified hour and zero minutes.
   *
   * @param hour the hour value
   * @throws IllegalArgumentException if hour value is negative
   */
  public Clock(int hour) throws IllegalArgumentException {
    setHour(hour);
    setMinute(0);
  }

  /**
   * Constructs a new Clock object at zero hours and zero minutes.
   */
  public Clock() {
    setHour(0);
    setMinute(0);
  }

  // getters and setters
  public int getHour() {
    return hour;
  }

  public int getMinute() {
    return minute;
  }

  /**
   * Sets the hour of this clock. The value is clamped between 0 and 23.
   *
   * @param hour the hour value
   * @throws IllegalArgumentException if hour value is negative
   */
  public void setHour(int hour) throws IllegalArgumentException {
    this.hour = Validate.that(hour, "hour", Validate.isNotNegative, "Cannot be negative") % 24;
  }

  /**
   * Sets the minute value of this clock. The value is clamped between 0 and 59.
   *
   * @param minute the minute value
   * @throws IllegalArgumentException if minute value is negative
   */
  public void setMinute(int minute) throws IllegalArgumentException {
    this.minute = Validate.that(
      minute, "minute", Validate.isNotNegative, "Cannot be negative"
    ) % 60;
  }

  // methods
  /**
   * Adds the specified hour and minute values to this clock and returns a new Clock object with 
   * that time. Nether of the Clocks are modified.
   *
   * @param hour the hour value
   * @param minute the minute value
   * @return a new Clock object with the specified time added to the time of this Clock
   * @throws IllegalArgumentException if new times for hour or minutes are negative
   */
  public Clock addTime(int hour, int minute) throws IllegalArgumentException {
    return new Clock(this.hour + hour, this.minute + minute);
  }

  /**
   * Tests if this Clock is before other Clock.
   *
   * @param other the other Clock
   * @return true if this Clock is before other Clock
   * @throws IllegalArgumentException if other is null
   */
  public boolean isBefore(Clock other) throws IllegalArgumentException {
    Validate.that(other, "other", Validate.isNotNull, "Cannot be null");
    return this.hour < other.hour || (this.hour == other.hour && this.minute < other.minute);
  }

  /**
   * Test if this Clock is equal to other Clock.
   *
   * @param object downcasted to other Clock
   * @return true if this Clock is equal to other Clock
   * @throws ClassCastException if object cannot be downcasted to Clock
   */
  @Override
  public boolean equals(Object object) throws ClassCastException { 
    Clock other = (Clock) object;
    if (this.hour != other.getHour()) {
      return false;
    }
    if (this.minute != other.getMinute()) {
      return false;
    }
    return true;
  }

  /**
   * Finds a hash code value for this Clock.
   *
   * @return a int hash code value for this Clock
   */
  @Override
  public int hashCode() {
    int result = hour * 100 + minute;
    return result;
  }

  // toString
  /**
   * Creates a string representation of this Clock.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return String.format("%02d:%02d", hour, minute);
  }
}
