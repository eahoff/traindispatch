package edu.ntnu.stud.models;

import edu.ntnu.stud.util.InOut;
import edu.ntnu.stud.util.Validate;

/**
 * <h2>Departure</h2>
 * Represents a train departure with a scheduled departure time, line, track, ID, destination, 
 * delay and track information.
 * <p>
 * <b>Note:</b> This class throws an IllegalArgumentException if the track number is negative and 
 * not -1 or if the ID is negative. A track number of -1 indicates that the track is undefined.
 * </p>
 * <h3>Responsibilities:</h3>
 * <ul>
 *     <li>Store and hanlde the departure time, line, track, ID, destination, and delay of a train 
 *     departure.</li>
 *     <li>Provide a representation of the departure.</li>
 *     <li>Ensure the fields are within the valid range.</li>
 * </ul>
 *
 * @author Erik Hoff
 * @version 1.0
 */
public class Departure {
  private final Clock departure;
  private final String line;
  private final int id;
  private final String destination;
  private int track;
  private Clock delay;

  // constructors
  /**
   * Creates a new Departure object with the values according to the following parameters.
   *
   * @param departure the scheduled departure time
   * @param line the line name and number
   * @param id the unique id of the train
   * @param destination the destination of the train
   * @param track the track number
   * @param delay the delay of the train
   * @throws IllegalArgumentException if id is negative, track is below -1 or if either clock is 
   *      null
   */
  public Departure(
      Clock departure, 
      String line, 
      int id, 
      String destination, 
      int track, 
      Clock delay
  ) throws IllegalArgumentException {
    this.departure = (Clock) Validate.that(
      departure, "departure", Validate.isNotNull, "departure cannot be null"
    );
    this.line = line;
    this.id = Validate.that(id, "id", Validate.isPositive, "id must be positive");
    this.destination = destination;
    setTrack(track);
    setDelay(delay);
  }

  /**
   * Creates a new Departure object with the values according to the following parameters.
   * Track is set to -1, indicating that the track is undefined.
   *
   * @param departure the scheduled departure time
   * @param line the line name and number
   * @param id the unique id of the train
   * @param destination the destination of the train
   * @param delay the delay of the train
   * @throws IllegalArgumentException if id is negative or if either clock is null
   */
  public Departure(
      Clock departure, String line, int id, String destination, Clock delay
  ) throws IllegalArgumentException {
    this.departure = (Clock) Validate.that(
      departure, "departure", Validate.isNotNull, "departure cannot be null"
    );
    this.line = line;
    this.id = Validate.that(id, "id", Validate.isPositive, "id must be positive");
    this.destination = destination;
    setTrack(-1);
    setDelay(delay);
  }

  /**
   * Creates a new Departure object with the values according to the following parameters.
   * Delay is set to 00:00, indicating that the train is not delayed.
   *
   * @param departure the scheduled departure time
   * @param line the line name and number
   * @param id the unique id of the train
   * @param destination the destination of the train
   * @param track the track number
   * @throws IllegalArgumentException if id is negative, track is below -1 or if the clock is null
   */
  public Departure(
      Clock departure, String line, int id, String destination, int track
  ) throws IllegalArgumentException {
    this.departure = (Clock) Validate.that(
      departure, "departure", Validate.isNotNull, "departure cannot be null"
    );
    this.line = line;
    this.id = Validate.that(id, "id", Validate.isPositive, "id must be positive");
    this.destination = destination;
    setTrack(track);
    setDelay(new Clock());
  }

  /**
   * Creates a new Departure object with the values according to the following parameters.
   * Track is set to -1, indicating that the track is undefined, and delay is set to 00:00,
   * indicating that the train is not delayed.
   *
   * @param departure the scheduled departure time
   * @param line the line name and number
   * @param id the unique id of the train
   * @param destination the destination of the train
   * @throws IllegalArgumentException if id is negative or if the clock is null
   */
  public Departure(
      Clock departure, String line, int id, String destination
  ) throws IllegalArgumentException {
    this.departure = (Clock) Validate.that(
      departure, "departure", Validate.isNotNull, "departure cannot be null"
    );
    this.line = line;
    this.id = Validate.that(id, "id", Validate.isNotNegative, "id cannot be negative");
    this.destination = destination;
    setTrack(-1);
    setDelay(new Clock());
  }

  // getters
  public Clock getScheduledDeparture() {
    return departure;
  }

  /**
   * Returns the actual departure time of this departure, including delay.
   *
   * @return the time
   */
  public Clock getDeparture() {
    return departure.addTime(delay.getHour(), delay.getMinute());
  }

  public String getLine() {
    return line;
  }

  public int getId() {
    return id;
  }
  
  public String getDestination() {
    return destination;
  }
 
  public int getTrack() {
    return track;
  }
  
  public Clock getDelay() {
    return delay;
  }

  // setters
  /**
   * Sets the track number of this departure.
   *
   * @param track the track number
   * @throws IllegalArgumentException if track is negative, other than -1
   */
  public void setTrack(int track) throws IllegalArgumentException {
    this.track = Validate.that(
      track, "track", arg -> arg >= -1, "track cannot be negative, other than -1"
    );
  }

  /**
   * Sets the delay of this departure.
   *
   * @param delay the delay of this departure
   * @throws IllegalArgumentException if delay is null
   */
  public void setDelay(Clock delay) throws IllegalArgumentException {
    this.delay = (Clock) Validate.that(
      delay, "delay", Validate.isNotNull, "delay cannot be nulll"
    );
  }

  // methods
  /**
   * Tests if this departure is delayed.
   *
   * @return true if this departure is delayed
   */
  public boolean isDelayed() {
    return delay.getHour() != 0 || delay.getMinute() != 0;
  }

  // toString
  /**
   * Creates a string representation of this departure.
   *
   * @retun the string
   */
  @Override
  public String toString() {
    String delayString = delay.getHour() == 0 && delay.getMinute() == 0 ? "" : delay.toString();
    String trackString = track == -1 ? "" : Integer.toString(track);
    return InOut.format(
      "| %9s | %4r | %4d | %15r | %5s | %5s |",  
      departure.toString(), line, Integer.toString(id), destination, delayString, trackString
    );
  }
}
