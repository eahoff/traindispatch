package edu.ntnu.stud.models;

import edu.ntnu.stud.util.Validate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * <h2>DispatchSystem</h2>
 * Represents a dispatch system in a transportation network. It is responsible for managing a list
 * of departures and a clock.
 * <p>
 * <b>Note:</b> The departures are stored in an ArrayList, which allows for efficient access and 
 * modification of departures. The departures are sorted by scheduled departure time and removed
 * when the departure time has passed.
 * </p>
 * <h3>Responsibilities:</h3>
 * <ul>
 *   <li>Keep track of all departures in the system.</li>
 *   <li>Provide methods to add, remove, and modify departures.</li>
 *   <li>Manage the time in the system through a Clock object.</li>
 * </ul>
 *
 * @author Erik Hoff
 * @version 1.0
 */
public class DispatchSystem {
  private final ArrayList<Departure> departures;
  private Clock clock;

  // constructors
  /**
   * Creates a new DispatchSystem object with the values according to the following parameters.
   *
   * @param departures the list of departures as an ArrayList of Departure objects
   * @param clock the clock
   * @throws IllegalArgumentException if any of the parameters are null
   */
  public DispatchSystem(
      ArrayList<Departure> departures, Clock clock
  ) throws IllegalArgumentException {
    this.departures = Validate.that(
        departures, "departures", arg -> arg != null, "departures cannot be null"
    );
    removeDuplicateId();
    setClock(clock);
    updateClock(clock);
    sortDepartures();
  }

  /**
   * Creates a new DispatchSystem object with the values according to the following parameters.
   *
   * @param departures the list of departures as a Departure array
   * @param clock the clock
   * @throws IllegalArgumentException if any of the parameters are null
   */
  public DispatchSystem(
      Departure[] departures, Clock clock
  ) throws IllegalArgumentException {
    this.departures = new ArrayList<Departure>(Arrays.asList(departures));
    removeDuplicateId();
    setClock(clock);
    updateClock(clock);
    sortDepartures();
  }

  /**
   * Creates a new DispatchSystem object with the values according to the following parameters.
   * The clock is set to zero hours and zero minutes.
   *
   * @param departures the list of departures as an ArrayList of Departure objects
   * @throws IllegalArgumentException if departures are null
   */
  public DispatchSystem(ArrayList<Departure> departures) throws IllegalArgumentException {
    this.departures = Validate.that(
        departures, "departures", arg -> arg != null, "departures cannot be null"
    );
    removeDuplicateId();
    setClock(new Clock());
    updateClock(clock);
    sortDepartures();
  }

  /**
   * Creates a new DispatchSystem object with the values according to the following parameters.
   * The clock is set to zero hours and zero minutes.
   *
   * @param departures the list of departures as a Departure array
   * @throws IllegalArgumentException if departures are null
   */
  public DispatchSystem(Departure[] departures) throws IllegalArgumentException {
    this.departures = new ArrayList<Departure>(Arrays.asList(departures));
    removeDuplicateId();
    setClock(new Clock());
    updateClock(clock);
    sortDepartures();
  }

  // getters and setters
  public ArrayList<Departure> getDepartures() {
    return departures;
  }

  /**
   * Returns the departure with the specified id.
   *
   * @param id the id of the departure beeing looked for
   * @return the departure with the specified id or null if no departure with the id exists
   */
  public Departure getDepartureById(int id) {
    Object[] res = departures.stream()
        .filter((d) -> (d.getId() == id))
        .collect(Collectors.toList())
        .toArray();
    if (res.length > 0) {
      return (Departure) res[0];
    } else {
      return null;
    }
  }

  /**
   * Returns the departures with the specified destination.
   *
   * @param destination the destination of the departures beeing looked for
   * @return all departures found with the specified destination or an empmty array if no departure 
   *         with the destination is found
   * @throws NullPointerException if destination is null
   */
  public Departure[] getDepartureByDestination(String destination) throws NullPointerException {
    return departures.stream()
        .filter((d) -> (d.getDestination().equals(destination)))
        .collect(Collectors.toList())
        .toArray(new Departure[0]);
  }

  public Clock getClock() {
    return clock;
  }

  /**
   * Sets the clock of this dispatch system.
   *
   * @param clock the clock
   * @throws IllegalArgumentException if clock is null
   */
  private void setClock(Clock clock) {
    this.clock = (Clock) Validate.that(
        clock, "clock", Validate.isNotNull, "clock cannot be null"
    );
  }

  // methods
  /**
   * Lookes for any duplicate id in the departures list and removes them. The first departure with
   * the id is kept according to the order of the list.
   */
  private void removeDuplicateId() {
    departures.stream()
        .collect(Collectors.groupingBy(Departure::getId)).values().stream()
        .filter((d) -> d.size() > 1).forEach((d) -> departures.removeAll(d.subList(1, d.size())));
  }

  /**
   * Adds a departure to the departures list. If the departure id already exists or is null, the 
   * prosses fails.
   *
   * @param departure the departure to be added
   * @return true if the departure was added, false if the prosses failed
   */
  public boolean addDeparture(Departure departure) {
    if (departure == null) {
      return false;
    }
    if (departures.stream()
        .filter((d) -> (d.getId() == departure.getId())).collect(Collectors.toList()).size() > 0) {
      return false;
    }
    departures.add(departure);
    sortDepartures();
    return true;
  }

  /**
   * Removes specified departure from the departures list.
   *
   * @param departure the departure to be removed
   * @return true if the departure was removed, false if the departure was not found
   */
  public boolean removeDeparture(Departure departure) {
    return departures.remove(departure);
  }

  /**
   * Removes the departure with specified id from the departures list.
   *
   * @param id the id of the departure to be removed
   * @return true if the departure was removed, false if the departure was not found
   */
  public boolean removeDepartureById(int id) {
    Departure find = getDepartureById(id);
    if (find != null) {
      return removeDeparture(find);
    }
    return false;
  }

  /**
   * Sorts the departures list by scheduled departure time.
   */
  private void sortDepartures() {
    departures.sort((d1, d2)
        -> d1.getScheduledDeparture().isBefore(d2.getScheduledDeparture()) ? -1 : 1);
  }

  /**
   * Updates the clock and the departures list according to the new clock time. All departures with
   * a departure time before the new clock time are removed. If the new clock time is before the
   * current clock time or null, the prosses fails.
   *
   * @param clock the new clock
   * @return true if the clock was updated, false if the prosses failed
   */
  public boolean updateClock(Clock clock) {
    if (clock == null || clock.isBefore(this.clock)) {
      return false;
    }
    this.clock = clock;
    removeBefore();
    return true;
  }

  /**
   * Removes all departures with a departure time before the current clock time.
   */
  private void removeBefore() {
    ArrayList<Departure> toRemove = new ArrayList<Departure>();
    departures.stream()
        .filter((d) -> d.getDeparture().isBefore(clock))
        .forEach((d) -> toRemove.add(d));
    departures.removeAll(toRemove);
  }

  // toString
  /**
   * Creates a string representation of this dispatch system.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(
          " ---------------------- "
        + "Time: (" + clock.toString() + ")"
        + " ----------------------\n");
    sb.append("| Departure | Line |  ID  |   Destination   | Delay | Track |\n");
    sb.append("|-----------|------|------|-----------------|-------|-------|\n");
    departures.stream().forEach((d) -> sb.append(d.toString() + "\n"));
    sb.append(" -----------------------------------------------------------\n");
    return sb.toString();
  }
}
