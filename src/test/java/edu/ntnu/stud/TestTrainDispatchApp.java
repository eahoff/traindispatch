package edu.ntnu.stud;

// imprt spesific functions instead of all
import static org.junit.Assert.assertEquals;

import edu.ntnu.stud.models.DispatchSystem;
import edu.ntnu.stud.ui.TrainDispatchApp;
import org.junit.Test;

/**
 * Test class for TrainDispatchApp.
 */
public class TestTrainDispatchApp {
  // test constructors
  @Test
  public void testConstructor() {
    TrainDispatchApp app = new TrainDispatchApp();
    assertEquals(false, app.getAutoSeed());
    app.setAutoSeed(true);
    assertEquals(true, app.getAutoSeed());
    assertEquals(false, app.getRunning());
    app.setRunning(true);
    assertEquals(true, app.getRunning());
    app = new TrainDispatchApp(true);
    assertEquals(true, app.getAutoSeed());
    assertEquals(false, app.getRunning());
  }

  // test methods
  @Test
  public void testSeed() {
    TrainDispatchApp app = new TrainDispatchApp();
    app.init();
    DispatchSystem ds = app.getData();
    assertEquals(0, ds.getDepartures().size());
    app = new TrainDispatchApp(true);
    app.init();
    ds = app.getData();
    assertEquals(10, ds.getDepartures().size());
  }
}
