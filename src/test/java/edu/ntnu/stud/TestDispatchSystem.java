package edu.ntnu.stud;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import edu.ntnu.stud.models.Clock;
import edu.ntnu.stud.models.Departure;
import edu.ntnu.stud.models.DispatchSystem;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;

/**
 * Test class for DispatchSystem.
 */
public class TestDispatchSystem {
  // Arrange
  Departure[] departuresFixtureExpected = {
    new Departure(new Clock(7, 0), "L3", 134, "Bergen", new Clock(0, 5)),
    new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", 3, new Clock(0, 0)),
    new Departure(new Clock(15, 45), "R11", 34, "Trondheim", 7, new Clock(2)),
    new Departure(new Clock(18, 0), "F34", 14, "Molde"),
  };
  Departure departureFixtureExpected = new Departure(new Clock(9, 9), "L1", 5, "Oslo S");
  Departure[] departuresFixtureExtreme = {
    new Departure(new Clock(7, 0), "L3", 134, "Bergen", new Clock(0, 5)),
    new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", 3, new Clock(0, 0)),
    new Departure(new Clock(15, 45), "R11", 34, "Trondheim", 7, new Clock(2)),
    new Departure(new Clock(18, 0), "F34", 14, "Molde"),
    new Departure(new Clock(18, 1), "F34", 14, "Molde"),
  };

  // test constructor
  @Test
  public void testConstructor_1_expected() {
    // Act
    DispatchSystem ds = new DispatchSystem(
        new ArrayList<>(Arrays.asList(departuresFixtureExpected)), 
        new Clock(5, 6)
    );
    // Assert
    assertEquals(4, ds.getDepartures().size());
    assertEquals(new Clock(5, 6), ds.getClock());
  }

  @Test
  public void testConstructor_1_extreme() {
    // Act
    DispatchSystem ds = new DispatchSystem(
        new ArrayList<>(Arrays.asList(departuresFixtureExtreme)), 
        new Clock(17, 6)
        );
    // Assert
    assertEquals(2, ds.getDepartures().size());
    assertEquals(new Clock(17, 6), ds.getClock());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_1_negative_1() {
    // Arrange
    ArrayList<Departure> departures = null;
    // Act and Assert
    new DispatchSystem(departures, new Clock(5, 6));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_1_negative_2() {
    // Arrange
    Clock clock = null;
    // Act and Assert
    new DispatchSystem(departuresFixtureExpected, clock);
  }

  @Test
  public void testConstructor_2_expected() {
    // Act
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected, new Clock(5, 6));
    // Assert
    assertEquals(4, ds.getDepartures().size());
    assertEquals(new Clock(5, 6), ds.getClock());
  }

  @Test
  public void testConstructor_2_extreme() {
    // Act
    DispatchSystem ds = new DispatchSystem(departuresFixtureExtreme, new Clock(17, 6));
    // Assert
    assertEquals(2, ds.getDepartures().size());
    assertEquals(new Clock(17, 6), ds.getClock());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_2_negative() {
    // Arrange
    Clock clock = null;
    // Act and Assert
    new DispatchSystem(departuresFixtureExpected, clock);
  }

  @Test
  public void testConstructor_3_expected() {
    // Act
    DispatchSystem ds = new DispatchSystem(
        new ArrayList<>(Arrays.asList(departuresFixtureExpected))
    );
    // Assert
    assertEquals(4, ds.getDepartures().size());
    assertEquals(new Clock(), ds.getClock());
  }

  @Test
  public void testConstructor_3_extreme() {
    // Act
    DispatchSystem ds = new DispatchSystem(
        new ArrayList<>(Arrays.asList(departuresFixtureExtreme))
    );
    // Assert
    assertEquals(4, ds.getDepartures().size());
    assertEquals(new Clock(), ds.getClock());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_3_negative() {
    // Arrange
    ArrayList<Departure> departures = null;
    // Act and Assert
    new DispatchSystem(departures, new Clock(5, 6));
  }

  @Test
  public void testConstructor_4_expected() {
    // Act
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Assert
    assertEquals(4, ds.getDepartures().size());
    assertEquals(new Clock(), ds.getClock());
  }

  @Test
  public void testConstructor_4_extreme() {
    // Act
    DispatchSystem ds = new DispatchSystem(departuresFixtureExtreme);
    // Assert
    assertEquals(4, ds.getDepartures().size());
    assertEquals(new Clock(), ds.getClock());
  }


  // test getters and setters
  @Test
  public void testGetDepartureById_expected() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertEquals(departuresFixtureExpected[1], ds.getDepartureById(1234));
    assertEquals(null, ds.getDepartureById(-1));
    assertEquals(null, ds.getDepartureById(1366576327));
  }

  @Test
  public void testGetDepartureById_extreme() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExtreme);
    // Act and Assert
    assertEquals(null, ds.getDepartureById(1366576327));
  }

  @Test
  public void testGetDepartureByDestination_expected() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertArrayEquals(
      new Departure[] {departuresFixtureExpected[2]}, ds.getDepartureByDestination("Trondheim")
    );
    assertArrayEquals(new Departure[] {}, ds.getDepartureByDestination(""));
  }

  @Test
  public void testGetDepartureByDestination_extreme() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExtreme);
    // Act and Assert
    assertArrayEquals(
        new Departure[] {}, 
        ds.getDepartureByDestination("%%%)=U&?=U%------------------------------"));
  }

  // test methods
  @Test
  public void testAddDeparture_expected() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertTrue(ds.addDeparture(departureFixtureExpected));
    assertEquals(5, ds.getDepartures().size());
    assertFalse(ds.addDeparture(departureFixtureExpected));
    assertEquals(5, ds.getDepartures().size());
  }

  @Test
  public void testAddDeparture_negative() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertFalse(ds.addDeparture(null));
  }

  @Test
  public void testRemoveDeparture_expected() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertTrue(ds.removeDeparture(departuresFixtureExpected[0]));
    assertEquals(3, ds.getDepartures().size());
    assertFalse(ds.removeDeparture(departuresFixtureExpected[0]));
    assertEquals(3, ds.getDepartures().size());
  }

  @Test
  public void testRemoveDepartureById_expected() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertEquals(4, ds.getDepartures().size());
    assertTrue(ds.removeDepartureById(134));
    assertEquals(3, ds.getDepartures().size());
    assertEquals(null, ds.getDepartureById(134));
    assertEquals(false, ds.removeDepartureById(134));
    assertEquals(3, ds.getDepartures().size());
  }

  @Test  
  public void testUpdateClock_expected() {
    // Arrange
    DispatchSystem ds = new DispatchSystem(departuresFixtureExpected);
    // Act and Assert
    assertTrue(ds.updateClock(new Clock(12, 34)));
    assertEquals(new Clock(12, 34), ds.getClock());
    assertEquals(3, ds.getDepartures().size());
    assertTrue(ds.updateClock(new Clock(12, 35)));
    assertEquals(2, ds.getDepartures().size());
    assertFalse(ds.updateClock(new Clock(12, 34)));
  }

  // toString
  @Test
  public void testToString() {
    // Arrange, Act and Assert
    assertEquals(
        " ---------------------- Time: (00:00) ----------------------\n"
        + "| Departure | Line |  ID  |   Destination   | Delay | Track |\n"
        + "|-----------|------|------|-----------------|-------|-------|\n"
        + "|   07:00   | L3   | 0134 | Bergen          | 00:05 |       |\n"
        + "|   12:34   | R10  | 1234 | Oslo S          |       |   3   |\n"
        + "|   15:45   | R11  | 0034 | Trondheim       | 02:00 |   7   |\n"
        + "|   18:00   | F34  | 0014 | Molde           |       |       |\n"
        + " -----------------------------------------------------------\n",
        new DispatchSystem(departuresFixtureExpected).toString());
  }
}
