package edu.ntnu.stud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import edu.ntnu.stud.models.Clock;
import edu.ntnu.stud.models.Departure;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for Departure.
 */
public class TestDeparture {
  // fixtures
  private Departure fixtureExpected;
  private Departure fixtureExtreme; 

  // Arrange
  @Before
  public void setUp() {
    fixtureExpected = new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", 3, new Clock(0, 5));
    fixtureExtreme = new Departure(new Clock(200, 34), "R10", 1234, "Oslo S", 3, new Clock(0, 5));
  }
  
  // test constructor
  @Test
  public void testConstructor_1_extreme() {
    // Arrange and Act
    Departure d = new Departure(new Clock(200, 34), "R10", 1234, "Oslo S", 3, new Clock(0, 5));
    // Assert
    assertEquals(new Clock(8, 34), d.getScheduledDeparture());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_1_negative_1() {
    // Arrange, Act and Assert
    new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", -2, new Clock(0, 5));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_1_negative_2() {
    // Arrange, Act and Assert
    new Departure(new Clock(12, 34), "R10", -1, "Oslo S", 2, new Clock(0, 5));
  }

  @Test
  public void testConstructor_2_expected() {
    // Arrange and Act
    Departure d = new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", new Clock(0, 5));
    // Assert
    assertEquals(-1, d.getTrack());
  }

  @Test
  public void testConstructor_2_extreme() {
    // Arrange and Act
    Departure d = new Departure(new Clock(200, 34), "R10", 1234, "Oslo S", new Clock(0, 5));
    // Assert
    assertEquals(new Clock(8, 34), d.getScheduledDeparture());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_2_negative() {
    // Arrange, Act and Assert
    new Departure(new Clock(12, 34), "R10", -1, "Oslo S", new Clock(0, 5));
  }

  @Test
  public void testConstructor_3_expected() {
    // Arrange and Act
    Departure d = new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", 2);
    // Assert
    assertEquals(new Clock(), d.getDelay());
  }

  @Test
  public void testConstructor_3_extreme() {
    // Arrange and Act
    Departure d = new Departure(new Clock(200, 34), "R10", 1234, "Oslo S", 2);
    // Assert
    assertEquals(new Clock(8, 34), d.getDeparture());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_3_negative_1() {
    // Arrange, Act and Assert
    new Departure(new Clock(12, 34), "R10", -1, "Oslo S", 2);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_3_negative_2() {
    // Arrange, Act and Assert
    new Departure(new Clock(12, 34), "R10", 1, "Oslo S", -2);
  }

  @Test
  public void testConstructor_4_expected() {
    // Arrange and Act
    Departure d = new Departure(new Clock(12, 34), "R10", 1234, "Oslo S");
    // Assert
    assertEquals(-1, d.getTrack());
    assertEquals(new Clock(), d.getDelay());
  }

  @Test
  public void testConstructor_4_extreme() {
    // Arrange and Act
    Departure d = new Departure(new Clock(200, 34), "R10", 1234, "Oslo S");
    // Assert
    assertEquals(new Clock(8, 34), d.getScheduledDeparture());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_4_negative() {
    // Arrange, Act and Assert
    new Departure(new Clock(12, 34), "R10", -1, "Oslo S", new Clock(0, 5));
  }

  // test getters and setters
  @Test
  public void testGetDeparture_expected() {
    // Arrange, Act and Assert
    assertEquals(new Clock(12, 39), fixtureExpected.getDeparture());
  }

  @Test
  public void testGetDeparture_extreme() {
    // Arrange, Act and Assert
    assertEquals(new Clock(8, 39), fixtureExtreme.getDeparture());
  }

  @Test
  public void testSetTrack_expected() {
    // Arrange and Act
    fixtureExpected.setTrack(2);
    // Assert
    assertEquals(2, fixtureExpected.getTrack());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetTrack_negative() {
    // Arrange, Act and Assert
    fixtureExpected.setTrack(-2);
  }

  @Test
  public void testSetDelay_expected() {
    // Arrange and Act
    fixtureExpected.setDelay(new Clock(0, 10));
    // Assert
    assertEquals(new Clock(0, 10), fixtureExpected.getDelay());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetDelay_negative() {
    // Arrange, Act and Assert
    fixtureExpected.setDelay(new Clock(-1, 10));
  }

  // test methods
  @Test  
  public void testIsDelayed_expected_1() {
    // Act and Assert
    assertTrue(fixtureExpected.isDelayed());
  }

  @Test
  public void testIsDelayed_expected_2() {
    // Arrange
    Departure d = new Departure(new Clock(12, 34), "R10", 1234, "Oslo S", 3, new Clock(0, 0));
    // Act and Assert
    assertFalse(d.isDelayed());
  }

  // toString
  @Test
  public void testToString_expected() {
    // Arrange, Act and Assert
    assertEquals(
        "|   02:25   | L6   | 0009 | Halden          | 00:44 |       |", 
        new Departure(new Clock(2, 25), "L6", 9, "Halden", new Clock(0, 44)).toString());
  }
}
