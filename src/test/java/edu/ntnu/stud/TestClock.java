package edu.ntnu.stud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue; 

import edu.ntnu.stud.models.Clock;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for Clock.
 */
public class TestClock {
  // fixtures
  private Clock fixtureExpexted;
  private Clock fixtureExtreme;

  // Arrange
  @Before
  public void setUp() {
    fixtureExpexted = new Clock(12, 34);
    fixtureExtreme = new Clock(464363312, 345334);
  }

  // test constructor
  @Test
  public void testConstructor_1_expected() {
    // Act and Assert
    assertEquals(12, fixtureExpexted.getHour());
    assertEquals(34, fixtureExpexted.getMinute());
  }

  @Test
  public void testConstructor_1_extreme() {
    // Act and Assert
    assertEquals(8, fixtureExtreme.getHour());
    assertEquals(34, fixtureExtreme.getMinute());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_1_negative_1() {
    // Act and Assert
    new Clock(-1, 1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_1_negative_2() {
    // Act and Assert
    new Clock(1, -1);
  }

  @Test
  public void testConstructor_2_expected() {
    // Arrange and Act
    Clock c = new Clock(12);
    // Assert
    assertEquals(12, c.getHour());
    assertEquals(0, c.getMinute());
  }

  @Test
  public void testConstructor_2_extreme() {
    // Arrange and Act
    Clock c = new Clock(464363312);
    // Assert
    assertEquals(8, c.getHour());
    assertEquals(0, c.getMinute());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_2_negative_1() {
    // Arrange, Act and Assert
    new Clock(1, -1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructor_2_negative_2() {
    // Arrange, Act and Assert
    new Clock(-1, 1);
  }

  // test getters and setters
  @Test
  public void testSetHour_expected() {
    // Act
    fixtureExpexted.setHour(23);
    // Assert
    assertEquals(23, fixtureExpexted.getHour());
    assertEquals(34, fixtureExpexted.getMinute());
  }

  @Test
  public void testSetHour_extreme() {
    // Act
    fixtureExpexted.setHour(464363312);
    // Assert
    assertEquals(8, fixtureExpexted.getHour());
    assertEquals(34, fixtureExpexted.getMinute());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetHour_negative() {
    // Act and Assert
    fixtureExpexted.setHour(-1);
  }

  @Test
  public void testSetMinute_expected() {
    // Act
    fixtureExpexted.setMinute(59);
    // Assert
    assertEquals(12, fixtureExpexted.getHour());
    assertEquals(59, fixtureExpexted.getMinute());
  }

  @Test
  public void testSetMinute_extreme() {
    // Act
    fixtureExpexted.setMinute(464363312);
    // Assert
    assertEquals(12, fixtureExpexted.getHour());
    assertEquals(32, fixtureExpexted.getMinute());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testSetMinute_negative() {
    // Act and Assert
    fixtureExpexted.setMinute(-1);
  }

  // test methods
  @Test  
  public void testAddTime_expected() {
    // Act
    Clock c = new Clock(12, 0).addTime(0, 34);
    // Assert
    assertEquals(fixtureExpexted, c); 
  }

  @Test
  public void testAddTime_extreme() {
    // Arrange and Act
    Clock expected = new Clock(20, 34);
    Clock result = new Clock(12, 0).addTime(464363312, 34654);
    // Assert
    assertEquals(expected, result);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testAddTime_negative() {
    // Act and Assert
    new Clock(12, 0).addTime(-1, -1);
  }

  @Test
  public void testIsBefore_expected() {
    // Arrange
    Clock c = new Clock(12, 0);
    // Act and Assert
    assertTrue(c.isBefore(new Clock(12, 34)));
    assertTrue(c.isBefore(new Clock(13, 0)));
    assertFalse(c.isBefore(new Clock(11, 0)));
    assertFalse(c.isBefore(new Clock(12, 0)));
  }

  // toString
  @Test
  public void testToString() {
    // Act and Assert
    assertEquals("12:34", fixtureExpexted.toString());
  }
}
