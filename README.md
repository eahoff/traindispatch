# Portfolio project IDATA1003 - 2023

STUDENT NAME = "Erik August Christensen Hoff"  
STUDENT ID = "111810"

## Project description

This project, is a console-based train dispatch application developed as a response to a protfolio project in the class IDATA1003 at NTNU. The application is designed to manage a list of train departures and a clock, allowing users to schedule departures, and perform other related operations through a user-friendly menu system. The project is structured into several classes, each with its specific role and responsibilities. These classes are documented in this readme file together with comprehensive diagrams to illustrate the flow, class structure, and use cases of the application. The source code for this project is available on the GitLab repository linked below.

## Project structure

### Flow diagram:
![Flow diagram](diagrams/svg/flowDiagram.svg "Flow diagram") <br>
This showes the general flow of the application. The user is presented with a menu of options. The user can then choose an option and the application will perform the corresponding operation. An example sequence of this can be seen below. The user can then choose another option or exit the application. 

### Class digram:
![Class diagram](diagrams/svg/classDiagram.svg "Class diagram") <br>
The fields and methods are marked with a + if they are public and a - if they are private. Dependencies are marked with a dotted line and indicates that a class uses another class internaly but does not stores or makes an instance of it in a field. Note that this diagram does not show boilerplate methods such as simple getters and setters as well as toString, hash, and equals methods. The utility class <code>Validate</code> have many fields serving as shorthand lambdas. To avoid cluttering the diagram, these are generelized as one field named <code>* predefinedCheck *</code>. The methods and fields not shown in the diagram are documented in the source code. Since types varies between theese functions they are given the generic notation T like that of the methods with generic types.

### Use case:
![Use case diagram](diagrams/svg/useCaseDiagram.svg "Use case diagram") <br>
This shows the use cases of the application.

### Sequence diagram example:
![Sequence diagram](diagrams/svg/sequenceDiagram.svg "Sequence diagram") <br>
This is a sequence diagram for case 2 of the switch in the TrainDispatchApp class. It represents the use case where a new departure is added to the dispatch sytem. This diagram does not show the interaction with the <code>System.out</code> and <code>Scanner</code> classes, but they are used in all methods called in the InOut class. The <code>System.out</code> class is used to print output to the console and the <code>Scanner</code> class is used to read input from the console.

### Despription of the classes:
<h4>Main</h4>
The <code>Main</code> class is the entry point of the application. It is responsible for 
initializing and starting the <code>TrainDispatchApp</code>.

<h4>TrainDispatchApp</h4>
The <code>TrainDispatchApp</code> class represents a train dispatch application. It is 
responsible for interacting with the user through a console-based menu system. The class 
manages the dispatch system and handles user input for dispatching trains. The role of this 
class is to serve as the main interface between the user and the dispatch 
system. It prompts the user for input and processes the input to perform various operations 
such as dispatching trains, scheduling departures, etc.

<h4>Clock</h4>
The <code>Clock</code> class represents a clock with hour and minute fields. It can also 
function as a time unit.
The values are clamped between 00:00 and 23:59, representing the 24-hour format of a day.

<h4>Departure</h4>
The <code>Departure</code> class represents a train departure with a scheduled departure time, 
line, track, ID, destination, delay and track information.

<h4>DispatchSystem</h4>
The <code>DispatchSystem</code> class represents a dispatch system in a transportation network.
It is responsible for managing a list of departures and a clock.

<h4>InOut</h4>
The <code>InOut</code> class is a utility class with static methods for improved input and 
output. The role of this class is to provide utility methods that can be used throughout the 
application. These methods are static, meaning they can be called without creating an instance 
of the <code>Utils</code> class.

<h4>Validate</h4>
The <code>Validate</code> class provides static methods for validating arguments. The validation 
is performed by passing a custom check to the <code>that</code> method or by passing in a 
predefined check.


## Link to repository

[GitLab](https://gitlab.stud.idi.ntnu.no/eahoff/traindispatch)

## How to run the project

To run the project, you can run the main method of the Main class in your IDE. You can also compile and package the project into a jar file using the mvn package. You can then run the jar file. See [this article](https://www.sohamkamani.com/java/cli-app-with-maven/) for instructions.

## How to run the tests

To run the tests, you can run the test methods of the Test classes in your IDE. You can also run the tests using the mvn package. See [this article](https://www.sohamkamani.com/java/cli-app-with-maven/) for instructions.

## References


The Validate class is based on a stackoverflow answer by the user Erk to [this](https://stackoverflow.com/questions/1809093/how-can-i-place-validating-constraints-on-my-method-input-parameters) question. All other code and documentation is written by me.
